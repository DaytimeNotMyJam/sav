import java.util.List;
class NumericSortingAlgorithm extends SortingAlgorithm {
    @Override
    void sort(List<Object> list) {
        for (int i = 1; i < list.size(); i++) {
            Object key = list.get(i);
            int j = i - 1;

            while (j >= 0 && compare(list.get(j), key) > 0) {
                list.set(j + 1, list.get(j));
                j--;
            }
            list.set(j + 1, key);

            // imprime el estado de cada paso
            System.out.println(list);
        }
    }

    private int compare(Object o1, Object o2) {
        Integer i1 = (Integer) o1;
        Integer i2 = (Integer) o2;
        return i1.compareTo(i2);
    }
}