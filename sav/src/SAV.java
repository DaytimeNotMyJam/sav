import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SAV {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el tipo de dato a usar (n para numeros, c para letras): ");
        String type = scanner.nextLine();

        // validar parametros
        DataType dataType;
        if ("n".equalsIgnoreCase(type)) {
            dataType = DataType.NUMERIC;
        }
        else if ("c".equalsIgnoreCase(type)) {
            dataType = DataType.CHARACTER;
        }
        else {
            System.out.println("Tipo de dato inválido. Use 'n' para números o 'c' para letras.");
            scanner.close();
            return;
        }

        System.out.print("Ingresar los valores separados por comas: ");
        String values = scanner.nextLine();

        //separamos los valores ingresados en un array
        String[] valueTokens = values.split(",");
        //iniciamos un list que acepta cualquier tipo que implemente Comparable
        List<Object> list = new ArrayList<>();
        //anadimos y transformamos los elementos de valueTokens a list como su debido tipo de dato
        try {
            if (dataType == DataType.NUMERIC) {
                for (String token : valueTokens) {
                    list.add(Integer.parseInt(token));
                }
            } else {
                for (String token : valueTokens) {
                    list.add(token.charAt(0));
                }
            }
        } catch (NumberFormatException e) {
            System.out.println("valores invalidos.");
            return;
        }

        SortingAlgorithm sortingAlgorithm;
        if (dataType == DataType.NUMERIC) {
            sortingAlgorithm = new NumericSortingAlgorithm();
        } else {
            sortingAlgorithm = new CharacterSortingAlgorithm();
        }

        sortingAlgorithm.sort(list);

        System.out.println("Tipo: " + dataType);
        System.out.println("Valores: " + list);

    }
}