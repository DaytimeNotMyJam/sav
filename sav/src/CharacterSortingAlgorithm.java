import java.util.Comparator;
import java.util.List;

class CharacterSortingAlgorithm extends SortingAlgorithm {
    @Override
    void sort(List<Object> list) {
        for (int i = 1; i < list.size(); i++) {
            Object key = list.get(i);
            int j = i - 1;

            while (j >= 0 && compare(list.get(j), key) > 0) {
                list.set(j + 1, list.get(j));
                j--;
            }
            list.set(j + 1, key);

            // imprime el estado de cada paso
            System.out.println(list);
        }
    }

    private int compare(Object o1, Object o2) {
        Character c1 = (Character) o1;
        Character c2 = (Character) o2;
        return c1.compareTo(c2);
    }
}