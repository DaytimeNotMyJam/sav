import java.util.List;

abstract class SortingAlgorithm {
    abstract void sort(List<Object> list);
}
